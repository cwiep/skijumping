#!/usr/bin/env ruby
# skijumping game in ruby

require 'rubygame'

$sjversion = 0.5
$sitting = 0
$moving = 1
$flying = 2
$landing = 3

# initialize
Rubygame.init
Rubygame::TTF.setup

#initialize random
rand

#clock
$clock = Rubygame::Clock.new
$clock.target_framerate = 30

#do screen stuff
screen = Rubygame::Screen.set_mode [800, 400]
screen.title = "Skijumping #{$sjversion}"
screen.fill [0, 0, 0]
screen.update

#doublebuffer
$dbuffer = Rubygame::Surface.new(screen.size)
$dbuffer.fill [255, 255, 255]

#load image function
def load_image(name, colorkey=nil)
    # Rubygame::Image.load has been replaced with Surface
	image = Rubygame::Surface.load_image(name)
	if colorkey != nil
		if colorkey == -1
			colorkey = image.get_at([0,0])
		end
		image.set_colorkey(colorkey)
	end
	return image, Rubygame::Rect.new(0,0,*image.size)
end

class Jumper
  attr_accessor :rect, :behaviour, :balance, :height, :distance
  
  def initialize
    @image, @rect = load_image("res/jumpermove.bmp", [255, 0, 255])
    @image2, @rect2 = load_image("res/jumperfly.bmp", [255, 0, 255])
    @image3, @rect3 = load_image("res/jumperland.bmp", [255, 0, 255])
    @image4, @rect4 = load_image("res/jumpersit.bmp", [255, 0, 255])
    @balance = 0
    @height = 100.0 
    @distance = 0
  end
  
  def startposition
    @rect.x = 0
    @rect.y = 0
    @behaviour = $sitting
    @height = 100.0
    @balance = 0
    @distance = 0
  end
  
  def start_moving
    @behaviour = $moving
  end
  
  def move
    if @height <= 0
      @behaviour = $landing
    end
    
    if @behaviour == $moving
      #picture moving
      @rect.x += 1
      @rect.y = move_function(@rect.x)
      @balance += rand(2)
      @balance = 100 if @balance >= 100
      if @rect.x >= 100
        @behaviour = $flying
        @height -= @balance.abs
      end
      
    elsif @behaviour == $flying
      #picture flying
      @rect.x += 5
      @rect.y = fly_function(@rect.x)
      @height -= 1
      
      if @height <= 0
        @behaviour = $landing
      end
      
      @distance = (@rect.x-100)/3
      
    elsif @behaviour == $landing
      #picture = landing
      @rect.x += 0
      #@rect.y = landing_function(@rect.x)
      @distance = (@rect.x-100)/3
    end
    
    
    if @rect.x > 800
      @behaviour = $sitting
      startposition()
    end
    
    @rect2 = @rect3 = @rect4 = @rect
    
  end
  
  def move_function(x)
    #(0.5*x-10)
    -0.000018*(x**3)+0.00001*(x**2)+1.3*x+10
  end
  
  def fly_function(x)
    0.4*x+70
  end
  
  def landing_function(x)
    x
  end
  
  def do_balance
    #@balance += 1
  end
  
  def draw
    case @behaviour
      when $sitting
        @image4.blit($dbuffer, @rect4)
      when $moving
        @image.blit($dbuffer, @rect)
      when $flying
        @image2.blit($dbuffer, @rect2)
      when $landing
        @image3.blit($dbuffer, @rect3)
    end
end
    
end

#load background
background, bgrect = load_image( "res/bg.bmp", -1 )

#font
schrift = Rubygame::TTF.new( "res/FreeSans.ttf" , 16 )

#event queue
queue = Rubygame::EventQueue.new

#create player
player = Jumper.new
player.startposition

#game's not over yet
game_over = false

while !game_over
  queue.each do |event|
    case event
      when Rubygame::ActiveEvent
        screen.update
      when Rubygame::QuitEvent
        game_over = true
      when Rubygame::KeyDownEvent
        case event.key
          when Rubygame::K_ESCAPE
            game_over = true
          when Rubygame::K_RIGHT
            player.balance -= 10
          when Rubygame::K_R
            player.startposition
          when Rubygame::K_LEFT
            player.balance += 10
          when Rubygame::K_SPACE
            player.start_moving
        end
    end
  end
  
  #time
  $clock.tick
  
  #move player
  player.move
  player.do_balance
  
  #draw height
  $dbuffer.fill [255, 255, 255]
  text = schrift.render("height:#{player.height}", true, [0, 0, 0])
  textpos = Rubygame::Rect.new( 0, 0, *text.size )
  textpos.centerx = $dbuffer.width/2
  text.blit($dbuffer, textpos)
  
  #draw balance
  text = schrift.render("balance:#{player.balance} (0 is perfect)", true, [0, 0, 0])
  textpos = Rubygame::Rect.new( 0, 20, *text.size )
  textpos.centerx = $dbuffer.width/2
  text.blit($dbuffer, textpos)
  
  #draw distance
  text = schrift.render("distance:#{player.distance}", true, [0, 0, 0])
  textpos = Rubygame::Rect.new( 0, 40, *text.size )
  textpos.centerx = $dbuffer.width/2
  text.blit($dbuffer, textpos)
  
  #draw instructions
  text = schrift.render("[space]=start, [r]=reset, [->]=balance", true, [0, 0, 0])
  textpos = Rubygame::Rect.new( 10, 360, *text.size )
  text.blit($dbuffer, textpos)
  
  #draw stuff on dbuffer
  background.blit($dbuffer, bgrect)
  player.draw
  
  #draw dbuffer on screen
  $dbuffer.blit(screen, [0,0])
  screen.update
end

#end
Rubygame.quit



# Skijumping #

A little game to test out ruby.
Created using ruby1.8 and rubygame.

## Version ##
* Version 0.5 2014-12-06
* Version 0.4 2014-02-19

## Controls ##
* [SPACE] = start moving
* while moving, hold balance with [ARROW KEY RIGHT] (0 is perfect, a non-zero
balance value will decrease your jump-distance)
* [R] = reset jumper to tower

## License ##
Licensed under MIT License. For more information see LICENSE.